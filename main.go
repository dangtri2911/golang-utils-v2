package main

import (
	"context"
	"fmt"

	"github.com/gofiber/fiber/v2"
	pkgApplication "gitlab.com/h162/golang-utils/application"
	pkgEventBus "gitlab.com/h162/golang-utils/eventbus"
)

func main() {
	fmt.Println("Welcome to our internal package")

	pkgApplication.Help()

	eventBus := pkgEventBus.NewRabbitMQ("localhost", "5672", "guest", "guest")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	app := fiber.New()

    app.Get("/", func(c *fiber.Ctx) error {
		pkgEventBus.TestFullFlow(eventBus, ctx)
        return c.SendString("Hello, World 👋!")
    })

    app.Listen(":3002")
}