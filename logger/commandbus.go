package pkgLogger

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"

	pkgPostgres "gitlab.com/h162/golang-utils/postgres"
)

type commandbusLogger struct {
	logger *logrus.Logger
	db *sqlx.DB
	tablename string
}

type commandBusLog struct {
	owner string
	action string
	command string
	payload string
}

type commandBusLogDB struct {
	id pkgPostgres.NullInt64 `db:"id"`
	level pkgPostgres.NullString `db:"level"`
	owner pkgPostgres.NullString `db:"owner"`
	action pkgPostgres.NullString `db:"action"`
	command pkgPostgres.NullString `db:"command"`
	payload pkgPostgres.NullString `db:"payload"`
	createdAt pkgPostgres.NullTime `db:"created_at"`
	updatedAt pkgPostgres.NullTime `db:"updated_at"`

}

func NewCommandBusLogger(db *sqlx.DB, tablename string) LoggerInterface{
	return &commandbusLogger{
		logger: logrus.New(),
		db: db,
		tablename: tablename,
	}	
}

func NewCommandBusLog(owner string, action string, command string, payload string) interface{}{
	return commandBusLog{
		owner: owner,
		action: action,
		command: command,
		payload: payload,
	}
}

func (l *commandbusLogger) SaveToDB(commandBusLog commandBusLog, logType string) (*commandBusLogDB, error){
	var commandBusLogDB commandBusLogDB

	fmt.Println(
		fmt.Sprintf(
			"INSERT INTO %v (owner, action, command, level, payload) VALUES ($1, $2, $3, $4, $5) RETURNING *",
			l.tablename, 
		),
		commandBusLog.owner,
		commandBusLog.action,
		commandBusLog.command,
		logType,		
		commandBusLog.payload,
	)

	err := l.db.QueryRowx(
		fmt.Sprintf(
			"INSERT INTO %v (owner, action, command, level, payload) VALUES ($1, $2, $3, $4, $5) RETURNING *",
			l.tablename, 
		),
		commandBusLog.owner,
		commandBusLog.action,
		commandBusLog.command,
		logType,		
		commandBusLog.payload,
	).StructScan(&commandBusLogDB)

	if err != nil {
		return nil, err
	}

	return &commandBusLogDB, nil

}

func (l *commandbusLogger) Info(verbose bool, data interface{}){
	commandBusLog := data.(commandBusLog)

	if verbose{
		logrus.Info(fmt.Sprintf("[CommandBus - %v - %v] %v", commandBusLog.owner, commandBusLog.command, commandBusLog.action))
	}

	l.SaveToDB(commandBusLog, "INFO")
}

func (l *commandbusLogger) Debug(verbose bool, data interface{}){
	commandBusLog := data.(commandBusLog)

	if verbose{
		logrus.Debug(fmt.Sprintf("[CommandBus - %v - %v] %v", commandBusLog.owner, commandBusLog.command, commandBusLog.action))
	}

	l.SaveToDB(commandBusLog, "DEBUG")
}

func (l *commandbusLogger) Error(verbose bool, data interface{}){
	commandBusLog := data.(commandBusLog)

	if verbose{
		logrus.Error(fmt.Sprintf("[CommandBus - %v - %v] %v", commandBusLog.owner, commandBusLog.command, commandBusLog.action))
	}

	l.SaveToDB(commandBusLog, "ERROR")
}
