module gitlab.com/dangtri2911/golang-utils-v2

go 1.13

require (
	github.com/gofiber/fiber/v2 v2.16.0
	github.com/google/uuid v1.3.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.2
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
	github.com/vardius/shutdown v1.0.2
	github.com/vardius/trace v1.0.1
	gitlab.com/h162/golang-utils v1.7.0
	go.mongodb.org/mongo-driver v1.7.1
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
)
